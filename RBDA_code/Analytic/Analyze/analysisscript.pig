cleaned_rent = load 'Inputprojtest/cleaned_rent.txt' as (address:chararray);
cleaned_rent_control_group = GROUP cleaned_rent ALL;
cleaned_rent_control_count = FOREACH cleaned_rent_control_group GENERATE COUNT(cleaned_rent);
STORE cleaned_rent_control_count INTO 'cleaned_rent_control_count';


cleaned_complaint = load 'Inputprojtest/cleaned_complaint.txt' as (address:chararray);
cleaned_complaint_group = GROUP cleaned_complaint ALL;
cleaned_complaint_count = FOREACH cleaned_complaint_group GENERATE COUNT(cleaned_complaint);
STORE cleaned_complaint_count INTO 'cleaned_complaint_count';


cleaned_restaurant = load 'Inputprojtest/cleaned_restaurant.txt' as (address:chararray);
cleaned_restaurant_group = GROUP cleaned_restaurant ALL;
cleaned_restaurant_count = FOREACH cleaned_restaurant_group GENERATE COUNT(cleaned_restaurant);
STORE cleaned_restaurant_count INTO 'cleaned_restaurant_count';


cleaned_vermin = load 'Inputprojtest/cleaned_vermin.txt' as (address:chararray);
cleaned_vermin_group = GROUP cleaned_vermin ALL;
cleaned_vermin_count = FOREACH cleaned_vermin_group GENERATE COUNT(cleaned_vermin);
STORE cleaned_vermin_count INTO 'cleaned_vermin_count';

rent_complaint_intersection = load 'Inputprojtest/rent_complaint_intersection.txt' as (address:chararray);
rent_complaint_intersection_group = GROUP rent_complaint_intersection ALL;
rent_complaint_intersection_count = FOREACH rent_complaint_intersection_group GENERATE COUNT(rent_complaint_intersection);
STORE rent_complaint_intersection_count INTO 'rent_complaint_intersection_count';

rent_vermin_intersection = load 'Inputprojtest/rent_vermin_intersection.txt' as (address:chararray);
rent_vermin_intersection_group = GROUP rent_vermin_intersection ALL;
rent_vermin_intersection_count = FOREACH rent_vermin_intersection_group GENERATE COUNT(rent_vermin_intersection);
STORE rent_vermin_intersection_count INTO 'rent_vermin_intersection_count';

restaurant_vermin_intersection = load 'Inputprojtest/restaurant_vermin_intersection.txt' as (address:chararray);
restaurant_vermin_intersection_group = GROUP restaurant_vermin_intersection ALL;
restaurant_vermin_intersection_count = FOREACH restaurant_vermin_intersection_group GENERATE COUNT(restaurant_vermin_intersection);
STORE restaurant_vermin_intersection_count INTO 'restaurant_vermin_intersection_count';

restaurant_rent_vermin_intersection = load 'Inputprojtest/restaurant_rent_vermin_intersection.txt' as (address:chararray);
restaurant_rent_vermin_intersection_group = GROUP restaurant_rent_vermin_intersection ALL;
restaurant_rent_vermin_intersection_count = FOREACH restaurant_rent_vermin_intersection_group GENERATE COUNT(restaurant_rent_vermin_intersection);
STORE restaurant_rent_vermin_intersection_count INTO 'restaurant_rent_vermin_intersection_count';


--PART 2 FILTER FOR ONLY MORE THAN 3 COMPLAINTS




rent_3complaint_intersection = load 'Inputprojtest/rent_complaint_intersection.txt' as (address:chararray, complaints:int);
rent_3complaint_intersection_filter = FILTER rent_3complaint_intersection BY complaints >= 3;
--dump rent_3complaint_intersection_filter;
rent_3complaint_intersection_group = GROUP rent_3complaint_intersection_filter ALL;
rent_3complaint_intersection_count = FOREACH rent_3complaint_intersection_group GENERATE COUNT(rent_3complaint_intersection_filter);
STORE rent_3complaint_intersection_count INTO 'rent_3complaint_intersection_count';

rent_3vermin_intersection = load 'Inputprojtest/rent_vermin_intersection.txt' as (address:chararray, complaints:int);
rent_3vermin_intersection_filter = FILTER rent_3vermin_intersection BY complaints >= 3;
rent_3vermin_intersection_group = GROUP rent_3vermin_intersection_filter ALL;
rent_3vermin_intersection_count = FOREACH rent_3vermin_intersection_group GENERATE COUNT(rent_3vermin_intersection_filter);
STORE rent_3vermin_intersection_count INTO 'rent_3vermin_intersection_count';


restaurant_3vermin_intersection = load 'Inputprojtest/restaurant_vermin_intersection.txt' as (address:chararray, complaints:int);
restaurant_3vermin_intersection_filter = FILTER restaurant_3vermin_intersection BY complaints >= 3;
restaurant_3vermin_intersection_group = GROUP restaurant_3vermin_intersection_filter ALL;
restaurant_3vermin_intersection_count = FOREACH restaurant_3vermin_intersection_group GENERATE COUNT(restaurant_3vermin_intersection_filter);
STORE restaurant_3vermin_intersection_count INTO 'restaurant_3vermin_intersection_count';


restaurant_rent_3vermin_intersection = load 'Inputprojtest/restaurant_rent_vermin_intersection.txt' as (address:chararray, complaints:int);
restaurant_rent_3vermin_intersection_filter = FILTER restaurant_rent_3vermin_intersection BY complaints >= 3;
restaurant_rent_3vermin_intersection_group = GROUP restaurant_rent_3vermin_intersection_filter ALL;
restaurant_rent_3vermin_intersection_count = FOREACH restaurant_rent_3vermin_intersection_group GENERATE COUNT(restaurant_rent_3vermin_intersection_filter);
STORE restaurant_rent_3vermin_intersection_count INTO 'restaurant_rent_3vermin_intersection_count';




