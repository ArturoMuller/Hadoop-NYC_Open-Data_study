import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class IntersectionMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

	private static final int MISSING = 9999;

	public static int getCount(String s) {
		String num;
		if ((num = s.substring(60)) != null) {
			int count;
			try {
				count = Integer.parseInt(num.trim());
				return count;
			} catch (NumberFormatException e) {
				// do nothing
			}
		}
		return -1;
	}


	@Override
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		String data = value.toString();
		context.write(new Text(data.substring(0, 60)), new IntWritable(getCount(data)));
	}

}