import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class IntersectionReducer extends Reducer<Text, IntWritable, Text, IntWritable> {

	@Override
	public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
		int count = -1;
		boolean have = false;
		for (IntWritable value : values) {
			if (count != -1) have = true;
			count = value.get();
		}
		if (have) context.write(key, new IntWritable(count));
	}
}