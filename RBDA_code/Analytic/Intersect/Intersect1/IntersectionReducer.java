import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class IntersectionReducer extends Reducer<Text, IntWritable, Text, IntWritable> {

	@Override
	public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
		boolean complaint, other;
		complaint = other = false;
		int count = -1;
		for (IntWritable value : values) {
			if (value.get() == -1) other = true;
			else {
				complaint = true;
				count = value.get();
			}
		}
		if (complaint && other) context.write(key, new IntWritable(count));
	}
}