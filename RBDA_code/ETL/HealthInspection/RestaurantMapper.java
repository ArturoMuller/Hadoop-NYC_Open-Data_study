import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class RestaurantMapper extends Mapper<LongWritable, Text, IntWritable, Text> {

	private static final int MISSING = 9999;

	public static boolean isInteger(String s) {
		for (char c : s.toCharArray()) {
			if (!Character.isDigit(c)) return false;
		}
		return s.length() > 0;
	}

	public static String extract(String s) {
		try {
			String val = s.substring(s.indexOf("\"", s.indexOf(":"))+1, s.lastIndexOf("\""));
			return val.replaceAll("[^a-zA-Z0-9\\s]+", "").trim();
		} catch (StringIndexOutOfBoundsException e) {
			//return null;
		}
		return null;
	}

	@Override
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		String line = value.toString();
		int open = line.indexOf("{")+1;
		int close = line.indexOf("}");
		line = line.substring(open, close);
		String[] fields = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1);
		if (fields.length == 4) {
			String num = extract(fields[1]);
			if (num != null && isInteger(num)) {
				String data = new String("{ "+fields[0].trim()+", "+fields[2].trim()+", "+fields[3].trim()+" } "+num);
				try {
					int val = Integer.parseInt(num);
					context.write(new IntWritable(val), new Text(data));
				} catch (NumberFormatException e) {
					//ignore
				}
			}
		}
	}

}