import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class ComplaintMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

	private static final int MISSING = 9999;

	public static String[] cleanAddress(String s) {
		String[] address = s.split("\\s+");
		for (int i = 1; i < address.length; i++) {
			if (Character.isDigit(address[i].charAt(address[i].length()-1))) {
				try {
					int num = Integer.parseInt(address[i].substring(address[i].length()-1, address[i].length()));
					if (num == 1 || num == 2 || num == 3) {
						if (address[i].length() > 1) {
							int numPre = Integer.parseInt(address[i].substring(address[i].length()-2, address[i].length()-1));
							if (numPre == 1) address[i] = new String(address[i]+"TH");
							else {
								if (num == 1) address[i] = new String(address[i]+"ST");
								else if (num == 2) address[i] = new String(address[i]+"ND");
								else if (num == 3) address[i] = new String(address[i]+"RD");
							}
						} else {
							if (num == 1) address[i] = new String(address[i]+"ST");
							else if (num == 2) address[i] = new String(address[i]+"ND");
							else if (num == 3) address[i] = new String(address[i]+"RD");
						}
					} else {
						address[i] = new String(address[i]+"TH");
					}
				} catch (NumberFormatException e) {
					//do nothing
				}
			}
			else if (i == 1 && address[i].equals("EAST") && address.length > 2) address[i] = new String("E");
			else if (i == 1 && address[i].equals("WEST") && address.length > 2) address[i] = new String("W");
			else if (i == 1 && address[i].equals("NORTH") && address.length > 2) address[i] = new String("N");
			else if (i == 1 && address[i].equals("SOUTH") && address.length > 2) address[i] = new String("S");
			else if (i > 1) {
				if (address[i].equals("STREET")) address[i] = new String("ST");
				else if (address[i].equals("AVENUE")) address[i] = new String("AVE");
				else if (address[i].equals("BOULEVARD")) address[i] = new String("BLVD");
				else if (address[i].equals("PLACE")) address[i] = new String("PL");
				else if (address[i].equals("SQUARE")) address[i] = new String("SQ");
				else if (address[i].equals("PLAZA")) address[i] = new String("PLZ");
				else if (address[i].equals("BUILDING")) address[i] = new String("BLDG");
				else if (address[i].equals("CENTER")) address[i] = new String("CTR");
				else if (address[i].equals("CIRCLE")) address[i] = new String("CIR");
				else if (address[i].equals("COURT")) address[i] = new String("CT");
				else if (address[i].equals("DRIVE")) address[i] = new String("DR");
				else if (address[i].equals("LANE")) address[i] = new String("LN");
				else if (address[i].equals("TERRACE")) address[i] = new String("TER");
				else if (address[i].equals("E")) address[i] = new String("EAST");
				else if (address[i].equals("W")) address[i] = new String("WEST");
				else if (address[i].equals("N")) address[i] = new String("NORTH");
				else if (address[i].equals("S")) address[i] = new String("SOUTH");
			}
		}
		return address;
	}

	public static boolean isInteger(String s) {
		for (char c : s.toCharArray()) {
			if (!Character.isDigit(c)) return false;
		}
		return s.length() > 0;
	}

	public static String extractValue(String s) {
		try {
			String val = s.substring(s.indexOf("\"", s.indexOf(":"))+1, s.lastIndexOf("\""));
			return val.replaceAll("[^a-zA-Z0-9\\s]+", "").trim();
		} catch (StringIndexOutOfBoundsException e) {
			//return null;
		}
		return null;
	}

	public static String extractKey(String s) {
		try {
			String key = s.substring(s.indexOf("\"")+1, s.indexOf(":")-1);
			return key.trim();
		} catch (StringIndexOutOfBoundsException e) {

		}
		return null;
	}

	@Override
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		String line = value.toString();
		line = line.substring(line.indexOf("\""), line.lastIndexOf("\"")+1);
		String[] fields = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1);
		String address, zipcode;
		address = zipcode = null;
		for (String field : fields) {
			if (extractKey(field).equals("incident_address")) address = extractValue(field);
			else if (extractKey(field).equals("incident_zip")) zipcode = extractValue(field);
		}
		if (address != null && zipcode != null && isInteger(zipcode) && zipcode.length() == 5) {
			String[] cleaned = cleanAddress(address.toUpperCase());
			if (isInteger(cleaned[0]) && cleaned[0].length() <= 5) {
				StringBuffer s = new StringBuffer();
				for (String clean : cleaned) s.append(clean+"_");
				s.append(zipcode);
				while(s.length() < 60) s.append("_");
				context.write(new Text(s.toString()), new IntWritable(1));
			}
		}
	}

}