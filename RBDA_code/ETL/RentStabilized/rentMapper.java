import java.io.IOException;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.lang.*;
import java.io.*;

public class rentMapper extends Mapper<LongWritable, Text, Text, NullWritable> {

	private static final int MISSING = 9999;

	public static boolean isInteger(String s) {
		for (char c : s.toCharArray()) {
			if (!Character.isDigit(c)) return false;
		}
		return s.length() > 0;
	}

	public static String extract(String s) {
		try {
			String val = s.substring(s.indexOf("\"", s.indexOf(":"))+1, s.lastIndexOf("\""));
			return val.replaceAll("[^a-zA-Z0-9\\s]+", "").trim();
		} catch (StringIndexOutOfBoundsException e) {
			//return null;
		}
		return null;
	}

	@Override
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
//use a second Scanner to parse the content of each line 
        Scanner scanner = new Scanner(value.toString());
        scanner.useDelimiter("\\t+");
        String zip = "";
        String building = "";
        String street = "";
        String streettype = "";
        String building2 = "";
        String street2 = "";
        String streettype2 = "";
        String underscore = "_";
        

        if (scanner.hasNext())
        {
            //assumes the line has a certain structure
            if (scanner.hasNext())// ZIP
            {
                zip = scanner.next();

            }
            if (scanner.hasNext()) // BLDGN01
            {
                building = scanner.next();
            }
            if (scanner.hasNext()) // STREET1
            {
                street = scanner.next();
                if(zip.contains("10009"))
                {
                    int c = 2;
                }
            }
            if (scanner.hasNext())//STSUFX1 BLDGNO2
            {    
                streettype = scanner.next();
                if(streettype.matches(".*\\d+.*") && !streettype.contains("TO"))
                {
                    String[] temp = streettype.split(" ");
                    if(temp.length >= 2)
                    {
                        streettype = temp[0];
                        int i = 1;
                        while(temp[i].isEmpty())
                        {
                            i++;
                        }
                        building2 = temp[i];
                    }
                    else
                    {
                        building2 = temp[0];
                        streettype = "";
                    }
                }
                else
                {
                    String[] temp = streettype.split(" ");
                    if(temp[0].matches(".*\\d+.*"))
                    {
                        building2 = streettype;
                        streettype = "";
                    }
                    else
                    {
                        streettype = temp[0];

                        for(int i = 1; i < temp.length; i++)
                        {
                            if(!temp[i].isEmpty())
                            {
                                building2 += " " + temp[i];
                            }
                        }
                    }
                    if(building2.contains("NEW") || streettype.contains("NEW"))
                    {
                        streettype = "";
                    }
                }
            }
            if(scanner.hasNext() && building2.isEmpty() && !streettype.contains("NEW"))
            {
                building2 = scanner.next();
            }
            if (scanner.hasNext() && !building2.contains("NEW")&& !streettype.contains("NEW"))
            {
                street2 = scanner.next();
            }
            if (scanner.hasNext()&& !building2.contains("NEW")&& !streettype.contains("NEW")) //fix this
            {
                String type = scanner.next();
                streettype2 = type;
                if(!type.matches(".*\\d+.*"))
                {

                    String[] temp = type.split(" ");
                    if(temp.length >= 1)  {
                        if(!temp[0].contains("NEW"))
                        {streettype2 = temp[0];}
                        else if(!type.contains("NEW")){
                            streettype2 = type;
                        }
                        else
                        {
                            streettype2 = "";
                        }
                    }
                }
            }

            /*extras*/
            if(building2.matches(".*\\d+.*") && !zip.contains("ZIP") && !building2.contains("NEW YORK"))
            {
                Scanner bldgtwo = new Scanner(building2);
                int bldg3 = 0;
                String streettemp = "";
                if(!building2.contains("TO"))
                {
                    while (bldgtwo.hasNext()) 
                    {

                        if (bldgtwo.hasNextInt() && bldg3 == 0)
                        {
                            bldg3 = bldgtwo.nextInt();
                        }
                        else if(bldg3 == 0)
                        {
                            streettemp = bldgtwo.next();
                        }
                        else
                        {
                            bldgtwo.next();
                        }
                    }

                    if(streettype2.isEmpty())
                    {
                        underscore = "";
                    }

                    try{
                        context.write (new Text(String.format("%-60.60s",building2.trim() + "_" + street2.trim() + underscore + streettype2.trim() + "_" + zip.trim()).replace(' ', '_') ), NullWritable.get());   
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else
                {   
                    /**** jkhkhk*/
                    int bldg1 = 0;
                    int bldg2 = 0;
                    boolean used = false;
                    String trash = ""; 
                    while (bldgtwo.hasNext()) 
                    {

                        if (bldgtwo.hasNextInt() && bldg1 == 0)
                        {
                            bldg1 = bldgtwo.nextInt();
                            used = true;
                        }
                        else if(bldgtwo.hasNextInt() && bldg2 == 0)
                        {
                            bldg2 = bldgtwo.nextInt();
                        }
                        else if(!used)
                        {
                            streettemp = bldgtwo.next();
                        }
                        else
                        {
                            bldgtwo.next();
                        }
                    }

                    if(streettype2.isEmpty())
                    {
                        underscore = "";
                    }

                    for(int i = bldg1; i <= bldg2; i = i + 2)
                    {  

                        
                        
                        try{
                             context.write(new Text(String.format("%-60.60s",i + "_" + street2.trim() + underscore + streettype2.trim() + "_" + zip.trim()).replace(' ', '_') ), NullWritable.get());      
                        }
                        catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    /**** jkhkhk*/
                }}
                underscore = "_";
            if(zip.contains("ZIP"))
            {
                return;  
            }
            

            


            else if(building.contains("TO"))
            {
                int bldg1 = 0;
                int bldg2 = 0;
                String trash = ""; 
                Scanner bldg = new Scanner(building);
                while (bldg.hasNext()) {
                    if (bldg.hasNextInt() && bldg1 == 0) 
                    {
                        bldg1 = bldg.nextInt();
                    }
                    else if(bldg.hasNextInt() && bldg2 == 0)
                    {
                        bldg2 = bldg.nextInt();
                    }
                    else
                    {
                        trash = bldg.next();
                    }
                }

                if(streettype.isEmpty())
                {
                    underscore = "";
                }

                
                for(int i = bldg1; i <= bldg2; i = i + 2)
                {  
                    try{
                        if(streettype.contains("NEW YORK")) { streettype = "";}
                        context.write(new Text(String.format("%-60.60s",i + "_" + street.trim() + underscore + streettype.trim() + "_" + zip.trim()).replace(' ', '_') ), NullWritable.get());    
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            else
            {
                if(streettype.isEmpty())
                {
                    underscore = "";
                }

                
                try{
                    if(streettype.contains("NEW YORK")) { streettype = "";}
                     context.write(new Text(String.format("%-60.60s",building.trim() + "_" + street.trim() + underscore + streettype.trim() + "_" +  zip.trim()).replace(' ', '_') ), NullWritable.get()); 
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }   

    }
	}


