Jeffrey Bour - jpb515@nyu.edu
Arturo Muller - aam760@nyu.edu
Srini Narasimharajan - sn938@nyu.edu

Realtime & Big Data Analytics Final Project
Professor Suzanne McIntosh
Sping 2016 Semester
May 3, 2016

RBDA_code contains all code written for our final project. There are two main components, ETL and Analytic:

ETL: A series of MapReduce jobs for data cleaning. HealthInspection and RentStabilized containt one job each. HealthInspection code formats addresses and removes duplicate addresses, RentStabilized code formats addresses and expands address ranges. ServiceRequest contains two jobs that format addresses and count the number of complaints per address. These jobs are identical, except for the type of complaints we filter in the reduce phase.

Analytic: Two folders, Intersect and Analyze. Intersect contains two MapReduce jobs, Intersect1 and Intersect2. Intersect1 finds the intersection of two (merged) lists of varying types (<Text, IntWritable> and <Text, Null>), Intersect2 finds the intersection of two (merged) lists of type <Text, IntWritable>. Analyze contains the Pig scripts used to analyze the resulting lists. 